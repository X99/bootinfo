#pragma once

#include <Arduino.h>

#ifdef ESP32
	#include <WiFi.h>
	#include <soc/rtc.h>
	#include <esp_partition.h>
#elif defined(ESP8266)
	#include <ESP8266WiFi.h>
#endif

#ifdef ESP32
#elif defined(ESP8266)
#endif


class BootInfo {
	private:
		String _projectBanner;
		uint8_t _headerWidth = 50;
		uint8_t _align = 20;

		//temporary vars
#ifdef ESP32
		esp_chip_info_t _chipInfos;
#elif defined(ESP8266)
#endif

		void _printHeader(String header);
		void _printSubHeader(String header);
		void _print(String text, boolean bold = false, boolean padded = false, char end='\n');
		String _calculateSize(uint64_t size);
		const String _getResetReasonText();
		void _printPartitions();
		uint8_t _strengthFromRSSI(int32_t rssi);
#ifdef ESP32
		String _wifiMode(wifi_mode_t w);
		String _flashChipMode(FlashMode_t mode);
#elif defined(ESP8266)
		String _wifiMode(WiFiMode_t w);
		String _flashChipVendor(uint8_t vendor);
		String _flashChipMode(uint8_t mode);
#endif

	public:
		BootInfo();
		void setProjectBanner(String projectBanner) {
			_projectBanner = projectBanner;
		}
		void setHeaderWidth(uint8_t width=50) {
			_headerWidth = width;
		}
		void setAlignement(uint8_t alignement=20) {
			_align = alignement;
		}

		void print();
};