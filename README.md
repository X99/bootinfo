<!--
To replace with correct values:
- BootInfo
- SONARTOKEN
- 2.1.0

Emojis: https://www.webfx.com/tools/emoji-cheat-sheet/
Record terminal: https://asciinema.org/
-->

<div align="center">
	<img src="logo.png" width="30%" alt="project's logo" style='fill: #94d31b'>
</div>

---

<div align="center">

__A simple console bootscreen for ESPs__

</div>

<div align="center">
    <img src='https://img.shields.io/badge/Version-2.1.0-red'/>
    <img src='https://img.shields.io/badge/ESP32-%E2%9C%93%20compatible-brightgreen?style=flat'>
    <img src='https://img.shields.io/badge/ESP01-%E2%9C%93%20compatible-brightgreen?style=flat'>
    <img src='https://img.shields.io/badge/ESP12E-%E2%9C%93%20compatible-brightgreen?style=flat'>
</div>

<div align="center">

<img src='https://img.shields.io/badge/Built%20with-C%2B%2B-blue?style=for-the-badge&logo=C%2B%2B'> <img src='https://img.shields.io/badge/Made%20with-LOVE-red?style=for-the-badge'>
</div>

# Table of content
[[_TOC_]]

# About BootInfo
## What is BootInfo
BootInfo is a tiny library made for the ESP32/ESP8266 lineup that displays, at boot time (or any time you want) a lot of informations about the microcontroller, such as its specs, memory chip size, partitions, WiFi informations...

Here are two examples. This one for the ESP32:

```
                  ___           ___                       ___
      ___        /  /\         /  /\          ___        /  /\
     /  /\      /  /:/_       /  /:/_        /  /\      /  /:/_
    /  /:/     /  /:/ /\     /  /:/ /\      /  /:/     /  /:/ /\
   /  /:/     /  /:/ /:/_   /  /:/ /::\    /  /:/     /  /:/ /::\
  /  /::\    /__/:/ /:/ /\ /__/:/ /:/\:\  /  /::\    /__/:/ /:/\:\
 /__/:/\:\   \  \:\/:/ /:/ \  \:\/:/~/:/ /__/:/\:\   \  \:\/:/~/:/
 \__\/  \:\   \  \::/ /:/   \  \::/ /:/  \__\/  \:\   \  \::/ /:/
      \  \:\   \  \:\/:/     \__\/ /:/        \  \:\   \__\/ /:/
       \__\/    \  \::/        /__/:/          \__\/     /__/:/
                 \__\/         \__\/                     \__\/
Put here a nice sentence describing your app



--========================== BOOT INFO ===========================--
Reset reason:              Reset due to power-on event.


--=========================== VERSION ============================--
Project Version:           1.00
ESP-IDF Version:           v3.2.3-14-gd3e562907


--============================= SOC ==============================--
SoC Model:                 ESP32
CPU Frequency:             240 MHz
Silicon revision:          1
Cores:                     2
On board radio:            WiFi , Bluetooth , BLE


--============================ MEMORY ============================--
Memory type:               External
Flash chip size:           4.00 MiB
Free heap size:            207.78 kiB
Min. free heap size:       205.05 kiB


--========================== PARTITIONS ==========================--
Type: APP       addr: 0x00010000  size: 1.25 MiB        label: app0
Type: OTA       addr: 0x00150000  size: 1.25 MiB        label: app1
Type: SPIFFS    addr: 0x00290000  size: 1.44 MiB        label: spiffs


--=========================== NETWORK ============================--
WiFi SSID:                 wifi_tests
WiFi MAC:                  3C:71:BF:59:84:D8
WiFi IP:                   192.168.144.2
Subnet mask:               255.255.255.0
Gateway:                   192.168.144.1
RSSI:                      -65dB (100%)
Hostname:                  espressif
```

And this one for the ESP8266

```
                  ___           ___                       ___
      ___        /  /\         /  /\          ___        /  /\
     /  /\      /  /:/_       /  /:/_        /  /\      /  /:/_
    /  /:/     /  /:/ /\     /  /:/ /\      /  /:/     /  /:/ /\
   /  /:/     /  /:/ /:/_   /  /:/ /::\    /  /:/     /  /:/ /::\
  /  /::\    /__/:/ /:/ /\ /__/:/ /:/\:\  /  /::\    /__/:/ /:/\:\
 /__/:/\:\   \  \:\/:/ /:/ \  \:\/:/~/:/ /__/:/\:\   \  \:\/:/~/:/
 \__\/  \:\   \  \::/ /:/   \  \::/ /:/  \__\/  \:\   \  \::/ /:/
      \  \:\   \  \:\/:/     \__\/ /:/        \  \:\   \__\/ /:/
       \__\/    \  \::/        /__/:/          \__\/     /__/:/
                 \__\/         \__\/                     \__\/
Put here a nice sentence describing your app



--========================== BOOT INFO ===========================--
Reset reason:              Normal system powerup
Boot mode:                 Normal mode


--=========================== VERSION ============================--
Project Version:           1.00
ESP-IDF Version:           2.2.2-dev(38a443e)
Full version:
SDK:2.2.2-dev(38a443e)
Core:2.7.1=20701000
lwIP:STABLE-2_1_2_RELEASE
glue:1.2-30-g92add50
BearSSL:5c771be


--============================= SOC ==============================--
CPU Frequency:             80 MHz


--============================ SKETCH ============================--
Sketch size:               275.97 kiB
Sketch MD5:                b6210dde43b3c073e10184dd330a52ad
Free sketch space:         2.73 MiB


--============================ MEMORY ============================--
Chip ID:                   1458415
Flash chip size:           4.00 MiB
Flash chip real size:      4.00 MiB
Flash chip speed:          40.00 MHz
Flash chip mode:           2
Flash CRC check:           Ok!
Heap fragmentation:        3%


--=========================== NETWORK ============================--
WiFi SSID:                 wifi_tests
WiFi MAC:                  18:FE:34:F4:7B:64
WiFi IP:                   192.168.144.3
Subnet mask:               255.255.255.0
Gateway:                   192.168.144.1
RSSI:                      -69dB (100%)
Hostname:                  ESP-F47B64
WiFi mode:                 station + access point
```


## Installation
### Using [PlatformIO](https://platformio.org/) (recommended)
Simply add `BootInfo` to your `platformio.ini`'s `lib_deps`:

```ini
lib_deps =
     # Using library Id
     7413

     # Using library Name
     BootInfo

     # Depend on specific version
     BootInfo@2.1.0

     # Semantic Versioning Rules
     BootInfo@^2.1.0
     BootInfo@~2.1.0
     BootInfo@>=2.1.0
```

You can also fetch the library from the command-line:

```shell
# Using library Id
$ platformio lib install 7413

# Using library Name
$ platformio lib install "BootInfo"

# Install specific version
$ platformio lib install 7413@2.1.0
$ platformio lib install "BootInfo@2.1.0"
```

### Using Arduino IDE

To use the library from Arduino IDE, download the project's ZIP file from the repo (see the "Clone" button up there :arrow_upper_right:) and unzip it into your Arduino `Libraries` folder.


## How to use it?
### Example
There is an example file in `examples/SimpleTest` folder that show how to use the library. Basically, here are the steps:

* include the library and declare a variable:
```C++
#include "bootinfo.h"
BootInfo b;
```
* set your project's banner. I like to use [PatorJK's ASCII art banner generator](http://patorjk.com/software/taag/). The banner should be stored in a multi-line `String`. There again, see example file.
```C++
b.setProjectBanner(banner);
```
* set header width. Headers are separators, their default width is 50 characters.
```C++
b.setHeaderWidth(55);
```
* set column alignement. All values (except partitions info) will be aligned to this column. The default column is 20.
```C++
b.setAlignement(27);
```
* finally, print everything out to the console.
```C++
b.print();
```

### Terminal/monitor output

When using PlatformIO, include `monitor_flags = --raw` to ensure output is correctly formatted.

### Project version

To set and display a correct "Project version", be sure to define `PROJECT_VER` somewhere, either in your `platformio.ini` using `build_flags = -DPROJECT_VER="1.0"` for example, or just a `#define`clause. This value must reflect **your** project's version, **not BootInfo's**.

## Licence
<div align="center">

![CC](https://mirrors.creativecommons.org/presskit/icons/cc.svg) ![BY](https://mirrors.creativecommons.org/presskit/icons/by.svg) ![SA](https://mirrors.creativecommons.org/presskit/icons/sa.svg)

</div>

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

## Support
If you find this code useful and appreciate my work, feel free to click the button below to donate and help me continue. Every cent is appreciated! Thank you! :ok_hand:
<div align="center">

[![](donate.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=6EX6SCMSYPF94&currency_code=EUR&source=url)

</div>


## Code quality

<div align="center">
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=BootInfo&metric=bugs'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=BootInfo&metric=code_smells'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=BootInfo&metric=coverage'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=BootInfo&metric=duplicated_lines_density'/>
</div>

<div align="center">
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=BootInfo&metric=ncloc'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=BootInfo&metric=sqale_rating'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=BootInfo&metric=alert_status'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=BootInfo&metric=reliability_rating'/>
</div>

<div align="center">
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=BootInfo&metric=security_rating'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=BootInfo&metric=sqale_index'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=BootInfo&metric=vulnerabilities'/>
</div>


# Credits
Project logo made by <a href="http://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>

# About the author

- [Twitter](https://twitter.com/therealX99)
- [Linkedin](https://www.linkedin.com/in/pierre-vernaeckt/)

<div align="center">
	<a href='https://pierre.vernaeckt.fr'>
		<img width='250' src='https://static.x99.fr/ULm9SWsQ6gzupZ.svg'>
	</a>
</div>

