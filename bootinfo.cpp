#include <bootinfo.h>

BootInfo::BootInfo() {
#ifdef ESP32
	esp_chip_info(&_chipInfos);
#elif defined(ESP8266)
#endif
}

void BootInfo::print() {
	Serial.print("\e[2J");

	_print(_projectBanner, true, true);

	_printHeader(String("BOOT INFO"));
	_print("Reset reason: ", true, true, '\0'); _print(_getResetReasonText());
#ifdef ESP32
#elif defined(ESP8266)
	_print("Boot mode:", true, true, '\0'); _print(ESP.getBootMode() == 0 ? "Enhance mode":"Normal mode");
	_print("Boot version:", true, true, '\0'); _print(String(ESP.getBootVersion()));
#endif




	_printHeader(String("VERSION"));
	_print("Project Version:", true, true, '\0'); _print(String(PROJECT_VER));
#ifdef ESP32
	_print("ESP-IDF Version:", true, true, '\0'); _print(String(ESP.getSdkVersion()));
#elif defined(ESP8266)
	_print("ESP-IDF Version:", true, true, '\0'); _print(String(ESP.getSdkVersion()));
	_print("Core version:", true, true, '\0'); _print(String(ESP.getCoreVersion()));
	String fullVersion = ESP.getFullVersion();
	fullVersion.replace("/","\n");
	_print("Full version:", true, true, '\n'); _print(fullVersion);
#endif




	_printHeader(String("SOC"));
#ifdef ESP32
	_print("SoC Model:", true, true, '\0'); 			_print(_chipInfos.model == CHIP_ESP32?"ESP32":"ESP32-S2");
_print("CPU Frequency:", true, true, '\0'); 			_print(String(getCpuFrequencyMhz())+" MHz");
	_print("Silicon revision:", true, true, '\0'); 		_print(String(_chipInfos.revision));
	_print("Cores:", true, true, '\0'); 				_print(String(_chipInfos.cores));
	_print("On board radio:", true, true, '\0'); 		_print(
		String(_chipInfos.features & CHIP_FEATURE_WIFI_BGN? "WiFi ":"")+
		String(_chipInfos.features & CHIP_FEATURE_BT? ", Bluetooth ":"")+
		String(_chipInfos.features & CHIP_FEATURE_BLE? ", BLE ":"")
	);
#elif defined(ESP8266)
	_print("CPU Frequency:", true, true, '\0'); 		_print(String(ESP.getCpuFreqMHz())+" MHz");
#endif




#ifdef ESP32
	_printHeader(String("SKETCH"));
	_print("Sketch size:", true, true, '\0'); 			_print(String(_calculateSize(ESP.getSketchSize())));
	_print("Sketch MD5:", true, true, '\0'); 			_print("0x" + String(ESP.getSketchMD5()));
	_print("Free sketch space:", true, true, '\0'); 	_print(String(_calculateSize(ESP.getFreeSketchSpace())));
#elif defined(ESP8266)
	_printHeader(String("SKETCH"));
	_print("Sketch size:", true, true, '\0'); 			_print(String(_calculateSize(ESP.getSketchSize())));
	_print("Sketch MD5:", true, true, '\0'); 			_print("0x" + String(ESP.getSketchMD5()));
	_print("Free sketch space:", true, true, '\0'); 	_print(String(_calculateSize(ESP.getFreeSketchSpace())));
#endif




	_printHeader(String("MEMORY"));
#ifdef ESP32
	_printSubHeader(String("Internal RAM"));
	_print("Heap size:", true, true, '\0'); 			_print(_calculateSize(ESP.getHeapSize()));
	_print("Free heap:", true, true, '\0'); 			_print(_calculateSize(ESP.getFreeHeap()));
	_print("Min. free heap size:", true, true, '\0'); 	_print(_calculateSize(ESP.getMinFreeHeap()));
	_print("Max. alloc heap:", true, true, '\0'); 		_print(_calculateSize(ESP.getMaxAllocHeap()));

	_printSubHeader(String("SPI RAM (PSRAM)"));
	if (ESP.getPsramSize() == 0) {
		_print("No PSRAM available");
	} else {
		_print("PSRAM size:", true, true, '\0'); 			_print(_calculateSize(ESP.getPsramSize()));
		_print("Free PSRAM:", true, true, '\0'); 			_print(_calculateSize(ESP.getFreePsram()));
		_print("Min. free PSRAM size:", true, true, '\0'); 	_print(_calculateSize(ESP.getMinFreePsram()));
		_print("Max. alloc PSRAM:", true, true, '\0'); 		_print(_calculateSize(ESP.getMaxAllocPsram()));
	}

	_printSubHeader(String("Flash chip"));
	_print("Memory type:", true, true, '\0'); 			_print((_chipInfos.features & CHIP_FEATURE_EMB_FLASH) ? "Embedded" :"External");
	_print("Flash chip size:", true, true, '\0'); 		_print(_calculateSize(ESP.getFlashChipSize()));
	_print("Flash chip speed:", true, true, '\0'); 		_print(String(ESP.getFlashChipSpeed()/1e6)+"MHz");
	_print("Flash chip mode:", true, true, '\0'); 		_print(_flashChipMode(ESP.getFlashChipMode()));
#elif defined(ESP8266)
	_print("Chip ID:", true, true, '\0'); 			_print("0x" + String(ESP.getFlashChipId(), HEX));
	int8_t flashChipVendor = ESP.getFlashChipId() & 0x000000ff;
	_print("Chip vendor:", true, true, '\0'); _print(_flashChipVendor(flashChipVendor));
	_print("Flash chip size:", true, true, '\0'); 	_print(_calculateSize(ESP.getFlashChipSize()));
	_print("Flash chip real size:", true, true, '\0'); _print(_calculateSize(ESP.getFlashChipRealSize()));
	_print("Flash chip speed:", true, true, '\0'); 	_print(String(ESP.getFlashChipSpeed()/1e6)+" MHz");
	_print("Flash chip mode:", true, true, '\0'); 	_print(_flashChipMode(ESP.getFlashChipMode()));
	_print("Flash CRC check:", true, true, '\0'); 	_print(ESP.checkFlashCRC()==true? "Ok!":"Failed");
	_print("Flash cfg check:", true, true, '\0'); 	_print(ESP.checkFlashConfig()==true? "Ok!":"Failed");
	_print("Heap fragmentation:", true, true, '\0'); _print(String(ESP.getHeapFragmentation())+"%");
#endif




#ifdef ESP32
	_printHeader(String("PARTITIONS"));
	_printPartitions();
#elif defined(ESP8266)
#endif




	_printHeader(String("NETWORK"));
	if (WiFi.isConnected()) {
		_print("WiFi SSID:", true, true, '\0'); 	_print(WiFi.SSID());
		_print("WiFi MAC:", true, true, '\0'); 		_print(WiFi.macAddress());
		_print("WiFi IP:", true, true, '\0'); 		_print(WiFi.localIP().toString());
		_print("Subnet mask:", true, true, '\0'); 	_print(WiFi.subnetMask().toString());
		_print("Gateway:", true, true, '\0'); 		_print(WiFi.gatewayIP().toString());
		_print("RSSI:", true, true, '\0'); 			_print(String(WiFi.RSSI())+"dB (" + _strengthFromRSSI(WiFi.RSSI()) + "%)");

#ifdef ESP32
		_print("Hostname:", true, true, '\0'); 		_print(String(WiFi.getHostname()));
#elif defined(ESP8266)
		_print("Hostname:", true, true, '\0'); 		_print(String(WiFi.hostname()));
		_print("WiFi mode:", true, true, '\0'); 	_print(_wifiMode(WiFi.getMode()));
#endif
	} else {
		_print("WiFi not connected\n", true, true);
	}

	_print("", true, true, '\0'); _print(String());
}

void BootInfo::_printHeader(String header) {
	String toPrint ="\n\n";
	uint16_t len = header.length()+2;

	uint16_t sideLength = (_headerWidth - len)/2;

	toPrint +="--";
	for(uint16_t i=2; i<sideLength - 1; i++) {
		toPrint +="=";
	}
	toPrint += (" " + header + " ");
	for(uint16_t i=1; i<sideLength - (len%2? 1:2); i++) {
		toPrint +="=";
	}
	toPrint +="--\n";

	Serial.print(toPrint);
}

void BootInfo::_printSubHeader(String header) {
	uint16_t len = header.length();

	header+=" ";
	for(uint16_t i=len+2; i<_headerWidth-1; i++) {
		header +="-";
	}
	Serial.println(header);
}

void BootInfo::_print(String text, boolean bold, boolean padded, char end) {
	if (padded == true) {
		uint16_t len = text.length();
		if (len < _align) {
			for(uint16_t i=len; i<_align; i++) {
				text += " ";
			}
		}
	}
	text+=end;
	if (bold == true) {
		Serial.print("\033[1m" + text +"\033[0m");
	} else {
		Serial.print(text);
	}
}

String BootInfo::_calculateSize(uint64_t size) {
    String hrSize ="";

    double b = size;
    double k = size/1024.0;
    double m = ((size/1024.0)/1024.0);
    double g = (((size/1024.0)/1024.0)/1024.0);
    double t = ((((size/1024.0)/1024.0)/1024.0)/1024.0);
    double p = (((((size/1024.0)/1024.0)/1024.0)/1024.0)/1024.0);

	//EiB not reprensented here because it overloads the 64bits integer.
    if (p > 1) {
        hrSize = String(t, 2) + String(" PiB");
    } else if (t > 1) {
        hrSize = String(t, 2) + String(" TiB");
    } else if (g > 1) {
        hrSize = String(g, 2) + String(" GiB");
    } else if (m > 1) {
        hrSize = String(m, 2) + String(" MiB");
    } else if (k > 1) {
        hrSize = String(k, 2) + String(" kiB");
    } else {
        hrSize = String(b, 2) + String(" B");
    }

    return hrSize;
}

const String BootInfo::_getResetReasonText() {
#if defined(ESP32)
	switch(esp_reset_reason()) {
		case ESP_RST_UNKNOWN: return "Reset reason cannot be determined"; break;
		case ESP_RST_POWERON: return "Reset due to power-on event."; break;
		case ESP_RST_EXT: return "Reset by external pin (not applicable for ESP32)"; break;
		case ESP_RST_SW: return "Software reset via restart."; break;
		case ESP_RST_PANIC: return "Software reset due to exception/panic."; break;
		case ESP_RST_INT_WDT : return "Reset (software or hardware) due to interrupt watchdog."; break;
		case ESP_RST_TASK_WDT: return "Reset due to task watchdog."; break;
		case ESP_RST_WDT: return "Reset due to other watchdogs."; break;
		case ESP_RST_DEEPSLEEP: return "Reset after exiting deep sleep mode."; break;
		case ESP_RST_BROWNOUT: return "Brownout reset (software or hardware)"; break;
		case ESP_RST_SDIO: return "Reset over SDIO."; break;
	}
	return "Reset for unknown reason.";
#elif defined(ESP8266)
	uint32  reason = system_get_rst_info()->reason;
	switch(reason) {
		case REASON_DEFAULT_RST: return "Normal system powerup"; break;
		case REASON_WDT_RST: return "Reset due to watchdogs."; break;
		case REASON_EXCEPTION_RST: return "Reset due to exception."; break;
		case REASON_SOFT_WDT_RST: return "Reset due to soft watchdogs."; break;
		case REASON_SOFT_RESTART: return "Reset due to watchdogs."; break;
		case REASON_DEEP_SLEEP_AWAKE: return "Reset due to sleep awake."; break;
		case REASON_EXT_SYS_RST: return "Reset due to system reset."; break;
	}
	return "Reset for unknown reason.";
#endif
}

void BootInfo::_printPartitions() {
#ifdef ESP32
	const esp_partition_t *partition = NULL;
	partition = esp_partition_find_first(ESP_PARTITION_TYPE_APP, ESP_PARTITION_SUBTYPE_APP_OTA_0, "app0");
	if (partition != NULL) {
		Serial.printf("Type: APP\taddr: 0x%08x  size: %s\tlabel: %s\n", partition->address, _calculateSize(partition->size).c_str(), partition->label);
	}

	partition = esp_partition_find_first(ESP_PARTITION_TYPE_APP, ESP_PARTITION_SUBTYPE_APP_OTA_1, "app1");
	if (partition != NULL) {
		Serial.printf("Type: OTA\taddr: 0x%08x  size: %s\tlabel: %s\n", partition->address, _calculateSize(partition->size).c_str(), partition->label);
	} else {
		Serial.printf("No OTA partition found !\n");
	}

	partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, (esp_partition_subtype_t)0x99, "eeprom");
	if (partition != NULL) {
		Serial.printf("Type: EEPROM\taddr: 0x%08x  size: %s\tlabel: %s\n", partition->address, _calculateSize(partition->size).c_str(), partition->label);
	}

	partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_SPIFFS, "spiffs");
	if (partition != NULL) {
		Serial.printf("Type: SPIFFS\taddr: 0x%08x  size: %s\tlabel: %s\n", partition->address, _calculateSize(partition->size).c_str(), partition->label);
	}
#elif defined(ESP8266)
#endif
}

uint8_t BootInfo::_strengthFromRSSI(int32_t rssi) {
	const int32_t dBm_max = -1;
	const int32_t dBm_min = -100;

	uint8_t s = 100 * (1 - (dBm_max - rssi) / (dBm_max - dBm_min));
	return s > 100? 100:(s < 0? 0:s);
}

#ifdef ESP32
String BootInfo::_wifiMode(wifi_mode_t w) {
	switch(w) {
		case WIFI_MODE_NULL: return "null"; break;
		case WIFI_MODE_STA: return "station"; break;
		case WIFI_MODE_AP: return "access point"; break;
		case WIFI_MODE_APSTA: return "station + access point"; break;
		default:break;
	}
	return "";
}
#elif defined(ESP8266)
String BootInfo::_wifiMode(WiFiMode_t w) {
	switch(w) {
		case WIFI_STA: return "station"; break;
		case WIFI_AP: return "access point"; break;
		case WIFI_AP_STA: return "station + access point"; break;
		case WIFI_OFF: return "off"; break;
		case WIFI_RESUME:
		case WIFI_SHUTDOWN:
			return "";
			break;
	}
	return "";

}
#endif

#if defined(ESP8266)
String BootInfo::_flashChipVendor(uint8_t vendor) {
	switch (vendor) {
		case 0xC8: return "GigaDevice"; break;
		case 0xEF: return "Winbond"; break;
		case 0xE0: return "BergMicro"; break;
		case 0x20: return "XMC"; break;
		default: break;
	}
	return "Unknown";
}
#endif

#if defined(ESP32)
String BootInfo::_flashChipMode(FlashMode_t mode) {
	switch (mode) {
		case FM_QIO: return "QIO"; break;
		case FM_QOUT: return "QOUT"; break;
		case FM_DIO: return "DIO"; break;
		case FM_DOUT: return "DOUT"; break;
		case FM_FAST_READ: return "Fast Read"; break;
		case FM_SLOW_READ: return "Slow Read"; break;
		default: break;
	}
	return "Unknown";
}
#elif defined(ESP8266)
String BootInfo::_flashChipMode(uint8_t mode) {
	switch (mode) {
		case FM_QIO: return "QIO"; break;
		case FM_QOUT: return "QOUT"; break;
		case FM_DIO: return "DIO"; break;
		case FM_DOUT: return "DOUT"; break;
		default: break;
	}
	return "Unknown";
}
#endif
