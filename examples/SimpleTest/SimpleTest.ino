#include "bootinfo.h"

BootInfo b;

String banner =
"                  ___           ___                       ___     \n" \
"      ___        /  /\\         /  /\\          ___        /  /\\    \n" \
"     /  /\\      /  /:/_       /  /:/_        /  /\\      /  /:/_   \n" \
"    /  /:/     /  /:/ /\\     /  /:/ /\\      /  /:/     /  /:/ /\\  \n" \
"   /  /:/     /  /:/ /:/_   /  /:/ /::\\    /  /:/     /  /:/ /::\\ \n" \
"  /  /::\\    /__/:/ /:/ /\\ /__/:/ /:/\\:\\  /  /::\\    /__/:/ /:/\\:\\\n" \
" /__/:/\\:\\   \\  \\:\\/:/ /:/ \\  \\:\\/:/~/:/ /__/:/\\:\\   \\  \\:\\/:/~/:/\n" \
" \\__\\/  \\:\\   \\  \\::/ /:/   \\  \\::/ /:/  \\__\\/  \\:\\   \\  \\::/ /:/ \n" \
"      \\  \\:\\   \\  \\:\\/:/     \\__\\/ /:/        \\  \\:\\   \\__\\/ /:/  \n" \
"       \\__\\/    \\  \\::/        /__/:/          \\__\\/     /__/:/   \n" \
"                 \\__\\/         \\__\\/                     \\__\\/\n" \
"Put here a nice sentence describing your app\n";

const char* ssid = "wifi_tests";
const char* password =  "o4ENAJT3mdR3ra75Mr";

void setup() {
	Serial.begin(115200);
	WiFi.begin(ssid, password);
	Serial.print("Connecting to WiFi..");
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}
	Serial.println("\nConnected!");

	b.setProjectBanner(banner);
	b.setHeaderWidth(70);
	b.setAlignement(27);

	b.print();
}

void loop() {

}